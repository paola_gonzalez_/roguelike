﻿using UnityEngine;
using System.Collections;

public abstract class MovingObject : MonoBehaviour {

	public float moveTime = 0.1f;

	private BoxCollider2D boxCollider;
	private Rigidbody2D rigidBody;
	private LayerMask collisionLayer;
	private float inverseMoveTime;
	
	protected virtual void Start () {
		boxCollider = GetComponent<BoxCollider2D> ();
		rigidBody = GetComponent<Rigidbody2D> ();
		collisionLayer = LayerMask.GetMask ("Collision Layer");
		inverseMoveTime = 1.0f / moveTime;
	}

	protected virtual void Move<T>(int xDirection, int yDirection){
		RaycastHit2D hit;
		bool canMove = CanObjectMove(xDirection, yDirection, out hit);

		if(canMove){
			return;
		}

		T hitComponent = hit.transform.GetComponent<T> ();

		if (hitComponent != null)  {
			HandleCollision(hitComponent);
		}
	}

	protected bool CanObjectMove(int xDirection, int yDriection, out RaycastHit2D hit){
		Vector2 startPosition = rigidBody.position;
		Vector2 endPostition = startPosition + new Vector2 (xDirection, yDriection);

		boxCollider.enabled = false;
		hit = Physics2D.Linecast (startPosition, endPostition, collisionLayer);
		boxCollider.enabled = true;

		if (hit.transform == null) {
			StartCoroutine (SmoothMovementRoutine (endPostition));
			return true;
		}

		return false;
	}

	protected IEnumerator SmoothMovementRoutine(Vector2 endPosition){
		float remainingDistanceToEndPostiton;

		do{
			remainingDistanceToEndPostiton = (rigidBody.position - endPosition).sqrMagnitude;
			Vector2 updatedPostiton = Vector2.MoveTowards(rigidBody.position, endPosition, inverseMoveTime * Time.deltaTime);
			rigidBody.MovePosition(updatedPostiton);
			yield return null;
		}
		while(remainingDistanceToEndPostiton > float.Epsilon);
	}

	protected abstract void HandleCollision<T>(T component);
}
